## Web Video Subtitler

(Inactive project)

This is a tiny program for desktop browsers, written in JavaScript and using HTML 5 technology. It lets you create SRT subtitles for videos your browser supports without using a server or remote service. You just have to select a video file your browser supports, start playing that video when you're ready and then set a start and end mark for a spoken phrase in the video. All this is done by just pressing the space bar.
After you have set an end mark the video is paused so that you can type the phrase that you just heard. Then you can continue until the video is finished and generate a SRT subtitle file which you can download from your browser.

Although this program runs in a browser you don't need to worry: None of the videos you edit in here is uploaded to a server. Feel free to check network requests in your browser's console. The only things that are loaded are this XHTML file, a CSS file and a JavaScript file.

If you like this program feel free to share it. It's released under the terms of the GNU Affero General Public License v3.

Have fun with Web Video Subtitler :)


Regards

Moritz Strohm
