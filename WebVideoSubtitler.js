/*
    Web Video Subtitler - a subtitle creator for desktop browsers
    Copyright (C) 2016  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


var new_subtitle_start = 0.0; //the start position (in seconds since video start) of a new subtitle (set by mark_new_subtitle)
var new_subtitle_end = 0.0; //the end position (in seconds since video start) of the new subtitle (set by mark_new_subtitle)

var app_state = 0; // 0: video stopped, 1: video playing, 2: subtitle start marked, 3: subtitle end marked + subtitle writing
//after state 3 (subtitle writing) state 1 follows. The user may save the subtitle any time


var subtitle_lines = []; //all subtitle lines belong here


function convert_time(time = 0.0) //float
{
    var str = time.toFixed(3).split("."); //SRT has the comma instead of the dot as decimal separator
    //we have to split time into seconds, minutes and hours:
    var seconds = Math.floor(time);
    //integer divisions:
    var minutes = Math.floor(seconds / 60); 
    var hours = Math.floor(minutes / 60).toString();
    minutes = (minutes % 60).toString();
    seconds = (seconds % 60).toString();
    if(hours.length==1)
    {
        //leading zero missing:
        hours = "0"+hours;
    }
    if(minutes.length==1)
    {
        minutes = "0"+minutes;
    }
    if(seconds.length==1)
    {
        seconds = "0"+seconds;
    }
    
    return hours+":"+minutes+":"+seconds+","+str[1];
}




function SubtitleLine()
{
    this.start_pos = 0.0;
    this.end_pos = 0.0;
    this.text = "";
    
    this.toString = function()
    {
        var str = convert_time(this.start_pos) + " --> " + convert_time(this.end_pos)+"\r\n";
        if(this.text.endsWith("\r\n"))
        {
            str += this.text+"\r\n"; //one newline already there from textarea text
        }
        else
        {
            str += this.text+"\r\n\r\n"; //no newline in textarea: add two newlines to leave an empty line between two subtitle items
        }
        
        
        return str;
    }
}



function show_subtitle_page()
{
    //var reader = new FileReader();
    //reader.readAsDataUrl(document.getElementById("VideoFileInput").value);
    //document.getElementById("Video").setAttribute("src", reader);
    
    document.getElementById("Video").src = URL.createObjectURL(document.getElementById("VideoFileInput").files[0]);
    document.getElementById("Video").load();
    document.getElementById("FileSelectPage").style.display = "none";
    document.getElementById("SubtitlePage").style.display = "block";
}


function append_subtitle_line()
{
    var new_subtitle_line = new SubtitleLine();
    new_subtitle_line.start_pos = new_subtitle_start;
    new_subtitle_line.end_pos = new_subtitle_end;
    new_subtitle_line.text = document.getElementById("NewSubtitle").value;
    subtitle_lines.push(new_subtitle_line);
    console.log("new subtitle line: "+new_subtitle_line.toString())
}


function create_srt_file()
{
    var output_string = "";
    for(var i = 0; i < subtitle_lines.length; i++)
    {
        output_string += (i+1).toString()+"\r\n"; //subtitle item number (from 1 to N)
        output_string += subtitle_lines[i].toString();
    }
    
    //the SRT file is ready to be sent to the user:
    //we have to encode the SRT file to Base64 first to preserve CR and LF characters
    document.getElementById("SRTDownloadLink").setAttribute("href", "data:text/plain;charset=utf-8;base64,"+window.btoa(output_string));
    document.getElementById("SRTDownloadLink").style.display = "inline";
}


window.onkeyup = function(e)
{
    if(e.keyCode == 32)
    {
        //space bar pressed
        //depending on app_state a different action is done
        switch(app_state)
        {
            case 0:
            {
                //video stopped: start playback
                document.getElementById("Video").play();
                app_state = 1;
                break;
            }
            case 1:
            {
                //video playback: set subtitle start mark
                new_subtitle_start = document.getElementById("Video").currentTime;
                app_state = 2;
                break;
            }
            case 2:
            {
                //subtitle start mark set: set subtitle end mark and enable textarea for subtitle text:
                new_subtitle_end = document.getElementById("Video").pause();
                new_subtitle_end = document.getElementById("Video").currentTime;
                document.getElementById("NewSubtitle").disabled = false;
                document.getElementById("GenerateButton").disabled = true;
                document.getElementById("GenerateButton").style.display = "none";
                document.getElementById("SRTDownloadLink").style.display = "none";
                document.getElementById("NewSubtitle").focus();
                app_state = 3;
                break;
            }
            default:
            {
                //nothing here. State 3 is left when CRTL is pressed
            }
        }
    }
    else if((e.keyCode == 17) && (app_state == 3))
    {
        //State 3 finished: Subtitle was entered. Append subtitle with start and end mark and resume playback and go to state 1
        document.getElementById("Video").play()
        document.getElementById("NewSubtitle").disabled = true;
        document.getElementById("GenerateButton").disabled = false;
        document.getElementById("GenerateButton").style.display = "inline";
        document.getElementById("Video").focus();
        append_subtitle_line();
        document.getElementById("NewSubtitle").value = "";
        app_state = 1;
    }
}